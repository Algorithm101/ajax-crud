$(function () {
    
    //read data from url
    $('#get-button').on('click', function () {
        $.ajax({
            url: "https://reqres.in/api/users",
            type: "GET",
            success: function(response){
                console.log(response)
                var tbodyEl = $('tbody');
                tbodyEl.html('');
                response.data.forEach(function(data){
                   tbodyEl.append('\
                                    <tr>\
                                        <td class="id">'+ data.id + '</td>\
                                        <td><input type="text" class="name" value="'+data.first_name+'"></td>\
                                        <td><button class="update-button">Update</button></td>\
                                        <td><button class="delete-button">Delete</button></td>\
                                    </tr>\
                    ');
                });
            }
        });
    });
    
    //create
    $('#create-form').on('submit', function(event){
        event.preventDefault;
        
        var createInput = $('#create-input');
        
        $.ajax({
            url: "https://reqres.in/api/users",
            method: 'POST',
            data: {
                name: createInput.val(),
                job: "leader"
            },
            success: function(response){
                console.log(response);
                console.log(createInput.val());
                createInput.val('');
                $('#get-button').click();
            }
            
        });
    });
    
    //update
    $('table').on('click','.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var newName = rowEl.find('.name').val();
    
        $.ajax({
            url:  "https://reqres.in/api/users/"+id,
            method: 'PUT',
            data: JSON.stringify({first_name:newName}),
            success: function(response){
                console.log(response);
                $('#get-button').click();
            }
        });
    });
    
    //detele
    $('table').on('click','.delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var newName = rowEl.find('.name').val();
    
        $.ajax({
            url:  "https://reqres.in/api/users/" + id,
            method: 'DELETE',
            success: function(response){
                console.log(response);
                $('#get-button').click();
            }
        });
    });
    
});
